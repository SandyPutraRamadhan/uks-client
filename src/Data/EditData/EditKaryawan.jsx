import React, { useState, useEffect } from "react";
import Swal from "sweetalert2";
import { Params, useNavigate, useParams } from "react-router-dom";
import axios from "axios";

export default function EditKaryawan() {
  const [nama, setNama] = useState("");
  const [tempatLahir, setTempatLahir] = useState("");
  const [tglLahir, setTglLahir] = useState("");
  const [alamat, setAlamat] = useState("");

  const param = new useParams();

  const navigate = useNavigate();

  const Put = async (e) => {
    e.preventDefault();

    try {
      await axios.put(`http://localhost:3089/data/` + param.id, {
        nama: nama,
        tempatLahir: tempatLahir,
        tglLahir: tglLahir,
        alamat: alamat,
        status : "KARYAWAN",
        jabatan: "karyawan"
      });
      Swal.fire({
        icon: "success",
        title: "Berhasil Mengedit",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        navigate("/karyawan");
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    axios
      .get("http://localhost:3089/data/" + param.id)
      .then((response) => {
        const karyawan = response.data.data;
        setNama(karyawan.nama);
        setTempatLahir(karyawan.tempatLahir);
        setTglLahir(karyawan.tglLahir);
        setAlamat(karyawan.alamat);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, [param.id]);
  return (
    <div>
      <div className="">
        <div className="py-10 px-[15rem]">
          <form className="space-y-3 shadow p-3 w-full" onSubmit={Put}>
            <h3 className="md:py-4 py-3 md:text-2xl text-xl font-medium text-black dark:text-black">
              Edit Karyawan
              <hr />
            </h3>
            <div className="flex gap-5">
              <div>
              <div>
                <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-gray-900 dark:text-black">
                  Nama Karyawan
                </label>
                <input
                  placeholder="Nama Karyawan"
                  value={nama}
                  onChange={(e) => setNama(e.target.value)}
                  className="bg-gray-50 mb-4 border w-[25rem] border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                />
              </div>
              <div>
                <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-gray-900 dark:text-black">
                  Tempat Lahir
                </label>
                <input
                  placeholder="Tempat Lahir"
                  value={tempatLahir}
                  onChange={(e) => setTempatLahir(e.target.value)}
                  className="bg-gray-50 md:mb-4 mb-3 border w-full border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                />
              </div>
              <div>
                <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-gray-900 dark:text-black">
                  Tanggal Lahir
                </label>
                <input
                  placeholder="Tanggal Lahir"
                  type="date"
                  value={tglLahir}
                  onChange={(e) => setTglLahir(e.target.value)}
                  className="bg-gray-50 md:mb-4 mb-3 border w-full border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                />
              </div>
              </div>

              <div>
                <div>
                  <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-gray-900 dark:text-black">
                    Alamat
                  </label>
                  <textarea
                    placeholder="alamat"
                    value={alamat}
                    onChange={(e) => setAlamat(e.target.value)}
                    className="bg-gray-50 md:mb-[4rem] mb-[1.5rem] border border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block w-[25rem] md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                    required
                  ></textarea>
                </div>
                <div className="md:flex gap-[3rem]">
                  <a
                    href="/karyawan"
                    className="w-[7rem] md:mr-10 mr-0 rounded float-right text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium text-sm ml-auto md:py-2.5 py-1.5 text-center no-underline dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                  >
                    Batal
                  </a>
                  <button
                    type="submit"
                    className="w-[7rem] md:float-right rounded text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium text-sm px-4 md:py-2.5 py-1.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    Simpan
                  </button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
