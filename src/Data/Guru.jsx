import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Sidebar1 from "../Component/Sidebar";
import axios from "axios";
import Navbar from "../Component/Navbar";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Swal from "sweetalert2";
import $ from "jquery";

export default function Guru() {
  const param = useParams();
  const [guru, setGuru] = useState([]);
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [tempatLahir, setTempatLahir] = useState("");
  const [tglLahir, setTglLahir] = useState("");
  const [excel, setExcel] = useState("");
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [show1, setShow1] = useState(false);

  const handleClose1 = () => setShow1(false);
  const handleShow1 = () => setShow1(true);

  const getAll = async () => {
    await axios
      .get(`http://localhost:3089/data/all?status=guru`)
      .then((res) => {
        setGuru(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAll(0);
  }, []);

  const add = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:3089/data", {
        nama: nama,
        alamat: alamat,
        status: "guru",
        tempatLahir: tempatLahir,
        tglLahir: tglLahir,
        jabatan: "guru",
      });
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Sukses Menambahkan Data Guru",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    } catch (error) {
      alert("Terjadi Kesalahan " + error);
    }
  };

  const deleteGuru = async (id) => {
    Swal.fire({
      title: "Apakah Anda Ingin Menghapus?",
      text: "Perubahan data tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Hapus",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`http://localhost:3089/data/` + id);
        Swal.fire({
          icon: "success",
          title: "Dihapus!",
          showConfirmButton: false,
          timer: 1500,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    });
  };

  const importExcell = async (e) => {
    e.preventDefault();

    const formData = new FormData();

    formData.append("file", excel);

    await axios
      .post(`http://localhost:3089/data/api/excel/upload/data`, formData)
      .then(() => {
        Swal.fire("Sukses!", " Berhasil Ditambahkan.", "success");
      })
      .catch((err) => {
        console.log(err);
        Swal.fire("Error", "Anda belum memilih file untuk diimport!.", "error");
      });
  };

  // download format data guru
  const downloadFormat = async () => {
    await Swal.fire({
      title: "Apakah Anda Ingin Mendownload?",
      text: "Ini file format excel untuk mengimport data",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#0b409c",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, download!",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios({
          url: `http://localhost:3089/data/api/excelTemplate/download/template?status=guru`,
          method: "GET",
          responseType: "blob",
        }).then((response) => {
          setTimeout(() => {
            var fileURL = window.URL.createObjectURL(new Blob([response.data]));
            var fileLink = document.createElement("a");

            fileLink.href = fileURL;
            fileLink.setAttribute("download", "format-guru.xlsx");
            document.body.appendChild(fileLink);

            fileLink.click();
          }, 2000);
        });
      }
    });
  };

  // download data guru
  const download = async () => {
    await Swal.fire({
      title: "Apakah Anda Ingin Mendownload?",
      text: "File berisi semua data guru",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#0b409c",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, download!",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios({
          url: `http://localhost:3089/data/api/excel/download/data?status=guru`,
          method: "GET",
          responseType: "blob",
        }).then((response) => {
          setTimeout(() => {
            var fileURL = window.URL.createObjectURL(new Blob([response.data]));
            var fileLink = document.createElement("a");

            fileLink.href = fileURL;
            fileLink.setAttribute("download", "guru.xlsx");
            document.body.appendChild(fileLink);

            fileLink.click();
          }, 2000);
        });
      }
    });
  };



  return (
    <div>
      <Navbar />
      <div className="flex p-10">
        <Sidebar1 />
        <div className="pl-64 w-full">
          <div className="shadow">
            <div className="flex gap-3 justify-between bg-cyan-600 pt-3 px-3 rounded-t-md  text-white">
              <h3>Daftar Guru</h3>
              <div className="ml-auto">
                <button
                  className="w-48 bg-green-500 hover:bg-blue-700 text-white font-bold py-1 px-2 rounded-full"
                  onClick={handleShow}
                >
                  Tambah
                </button>
              </div>
              <div>
                <button onClick={handleShow1} className="w-48 bg-green-500 hover:bg-blue-700 text-white font-bold py-1 px-2 rounded-full">
                  Import Data
                </button>
                <p className="mt-1">
                  <button
                    type="button"
                    onClick={download}
                    className="w-48 bg-green-500 hover:bg-blue-700 text-white font-bold py-1 px-2 rounded-full"
                  >
                    Download Data
                  </button>
                </p>
              </div>
            </div>
            {/* Table */}
            <div class="flex flex-col px-3">
              <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="inline-block min-w-full py-2 sm:px-6 lg:px-8">
                  <div class="overflow-hidden">
                    <table class="min-w-full text-left text-sm font-light">
                      <thead class="border-b bg-white font-medium dark:border-neutral-500 dark:bg-neutral-600">
                        <tr>
                          <th scope="col" class="px-6 py-3">
                            No.
                          </th>
                          <th scope="col" class="px-6 py-3">
                            Nama Guru
                          </th>
                          <th scope="col" class="px-6 py-3">
                            Tempat Tanggal Lahir
                          </th>
                          <th scope="col" class="px-6 py-3">
                            Alamat
                          </th>
                          <th scope="col" class="px-6 py-3">
                            Aksi
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        {guru.map((gurus, index) => {
                          return (
                            <tr
                              class="border-b bg-neutral-100 dark:border-neutral-500 dark:bg-neutral-700"
                              key={index.id}
                            >
                              <td class="whitespace-nowrap px-6 py-3 font-medium">
                                {index + 1}
                              </td>
                              <td class="whitespace-nowrap px-6 py-3">
                                {gurus.nama}
                              </td>
                              <td class="whitespace-nowrap px-6 py-3">
                                {gurus.tempatLahir} ,{gurus.tglLahir}
                              </td>
                              <td class="whitespace-nowrap px-6 py-3">
                                {gurus.alamat}
                              </td>
                              <td class="whitespace-nowrap px-6 py-3">
                                <button class="bg-green-600 hover:bg-green-700 font-bold py-1 px-2 rounded">
                                  <a href={"/editGuru/" + gurus.id}>
                                    <i class="fa-solid fa-pen-to-square text-white"></i>
                                  </a>
                                </button>
                                <button
                                  onClick={() => deleteGuru(gurus.id)}
                                  class="bg-red-600 ml-2 hover:bg-red-700 text-white font-bold py-1 px-2 rounded"
                                >
                                  <i class="fa-solid fa-trash-can"></i>
                                </button>
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* Modal Tambah */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Tambah Guru</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={add}>
            <div>
              <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-black dark:text-black">
                Nama Guru
              </label>
              <input
                placeholder="nama guru"
                value={nama}
                onChange={(e) => setNama(e.target.value)}
                className="bg-white border mb-3 border-gray-300 text-black md:text-base text-sm  focus:ring-blue-500 focus:border-blue-500 block w-full rounded md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
              ></input>
            </div>
            <label className="block md:mb-3 mb-2 text-sm font-medium text-black dark:text-black">
              Tempat Lahir
            </label>
            <input
              placeholder="Tempat Lahir"
              value={tempatLahir}
              onChange={(e) => setTempatLahir(e.target.value)}
              className="bg-white border  mb-3  w-full rounded border-gray-300 text-black text-sm  focus:ring-blue-500 focus:border-blue-500 block  md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
            <label className="block md:mb-3 mb-2 text-sm font-medium text-black dark:text-black">
              Tanggal Lahir
            </label>
            <input
              placeholder="Tanggal Lahir"
              type="date"
              value={tglLahir}
              onChange={(e) => setTglLahir(e.target.value)}
              className="bg-white border mb-3  w-full rounded border-gray-300 text-black text-sm  focus:ring-blue-500 focus:border-blue-500 block  md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
            <label className="block md:mb-3 mb-2 text-sm font-medium text-black dark:text-black">
              Alamat
            </label>
            <input
              placeholder="Alamat"
              value={alamat}
              onChange={(e) => setAlamat(e.target.value)}
              className="bg-white border md:mb-7 mb-3  w-full rounded border-gray-300 text-black text-sm  focus:ring-blue-500 focus:border-blue-500 block  md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Batal
              </Button>
              <Button variant="primary" type="submit">
                Simpan
              </Button>
            </Modal.Footer>
          </form>
        </Modal.Body>
      </Modal>

      {/* Modal Import */}
      <Modal show={show1} onHide={handleClose1}>
        <Modal.Header closeButton>
          <Modal.Title>Import Guru Dari File Excel</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <div className="rounded-md text-center shadow-md py-3 border">
            <p>
              Download file di bawah ini untuk menginput data guru <br />{" "}
              <span className="font-semibold">
                (*column tanggal lahir diubah menjadi date*)
              </span>
            </p>
            <Button variant="primary" onClick={downloadFormat}>
              Download Format File
            </Button>
          </div>
          <form className="mt-4" onSubmit={importExcell}>
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-40">
                Drop File.xlsx
              </label>
              <input
                type="file"
                required
                accept=".xlsx"
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-2 border-[#292929]"
                onChange={(e) => setExcel(e.target.files[0])}
              />
            </div>
            <div className="flex gap-4 mt-4 ml-[9rem]">
              <button
                onClick={handleClose1}
                className="w-full text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-4 py-2 text-center"
              >
                Batal
              </button>
              <button
                type="submit"
                className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 text-center"
              >
                Tambah
              </button>
            </div>
          </form>
        </Modal.Body>
      </Modal>
    </div>
  );
}
