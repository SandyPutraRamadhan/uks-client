import axios from "axios";
import React, { useState } from "react";
import { useEffect } from "react";

export default function Navbar() {
  const [profile, setProfile] = useState([]);

  const getAll = async () => {
    await axios
      .get("http://localhost:3089/uks/" + localStorage.getItem("userId"))
      .then((res) => {
        setProfile(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAll();
  }, []);

  let time = new Date().toLocaleTimeString();
  const [currentTime, setCurrentTime] = useState(time);

  const updateTime = () => {
    let time = new Date().toLocaleTimeString();
    setCurrentTime(time);
  };

  setInterval(updateTime, 1000);

  const d = new Date();
  const months = [
    "jan",
    "feb",
    "mar",
    "apr",
    "may",
    "jun",
    "jul",
    "aug",
    "sep",
    "oct",
    "nov",
    "dec",
  ];

  const month = months[d.getMonth()];
  const date = d.getDate();
  const year = d.getFullYear();
  return (
    <div className="pl-64">
      <nav class="sticky bg-white border-gray-200 dark:bg-gray-900 shadow">
        <div class="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto px-2">
          <div class="flex gap-3 items-center md:order-2">
            <p className="mt-3">
              {date}/{month}/{year}
            </p>
            <h4 className="text-cyan-600 mt-1 font-serif">{currentTime}</h4>
            <div className="flex gap-2 ml-auto md:ml-auto pr-4">
              {profile.foto ? (
                <a href="/prof">
                  <img
                    className=" mt-2 rounded-full w-[2.5rem] h-[2.5rem] border-black border"
                    src={profile.foto}
                    alt=""
                  />
                </a>
              ) : (
                <a href="/prof">
                  <img
                    className=" mt-2 rounded-full max-w-[2rem] h-[2rem] w-[2.5rem] border-black border"
                    src="https://static.vecteezy.com/system/resources/previews/008/442/086/original/illustration-of-human-icon-user-symbol-icon-modern-design-on-blank-background-free-vector.jpg"
                    alt=""
                  />
                </a>
              )}
            </div>
          </div>
          <div
            class="items-center justify-between hidden w-full md:flex md:w-auto md:order-1"
            id="mobile-menu-2"
          >
            <a
              href="/home"
              className="no-underline p-2 text-cyan-600 text-center"
            >
              <h1 className="text-xl font-semibold mt-3">SMPN 1 SEMARANG</h1>
            </a>
          </div>
        </div>
        <hr />
      </nav>
    </div>
  );
}
