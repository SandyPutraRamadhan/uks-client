import Sidebar from "./Sidebar";
import Navbar from "./Navbar";
import axios from "axios";
import React, { useState } from "react";
import { useEffect } from "react";
import { Button, Form, InputGroup, Modal } from "react-bootstrap";
import { useParams } from "react-router-dom";
import Swal from "sweetalert2";
import bcrypt from "bcryptjs";

export default function Profile() {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [show1, setShow1] = useState(false);
  const handleClose1 = () => setShow1(false);
  const handleShow1 = () => setShow1(true);

  const [profile, setProfile] = useState({
    email: "",
    password: "",
    username: "",
    noTelp: "",
    alamat: "",
    foto: null,
  });

  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [alamat, setAlamat] = useState("");
  const [noTelp, setNoTelp] = useState("");
  const [foto, setPhotoProfile] = useState(null);

  const getAll = async () => {
    await axios
      .get("http://localhost:3089/uks/" + localStorage.getItem("userId"))
      .then((res) => {
        setProfile(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    axios
      .get("http://localhost:3089/uks/" + localStorage.getItem("userId"))
      .then((response) => {
        const profil = response.data.data;
        setEmail(profil.email);
        setUsername(profil.username);
        setAlamat(profil.alamat);
        setNoTelp(profil.noTelp);
        setPhotoProfile(profil.foto);
        // setPassword(profil.password);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, []);

  useEffect(() => {
    getAll();
  }, []);

  const put = async (e) => {
    e.preventDefault();
    e.persist();

    const data = new FormData();
    data.append("file", foto);

    console.log(foto);
    try {
      await axios.put(
        `http://localhost:3089/uks/foto/${localStorage.getItem("userId")}`,
        data
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "berhasil mengedit",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (err) {
      console.log(err);
    }
  };

  const [passwordType, setPasswordType] = useState("password");
  const [passwordType1, setPasswordType1] = useState("password");
  const [passwordType2, setPasswordType2] = useState("password");
  const [passLama, setPassLama] = useState("");
  const [conPassLama, setConPassLama] = useState("");
  const [passBaru, setPassBaru] = useState("");
  const [conPassBaru, setConPassBaru] = useState("");

  const conPassword = async () => {
    await axios
      .get("http://localhost:3089/uks/" + localStorage.getItem("userId"))
      .then((res) => {
        setPassLama(res.data.data.password);
      });
  };
  useEffect(() => {
    conPassword();
  }, []);

  const ubahPass = (e) => {
    e.preventDefault();
    bcrypt.compare(conPassLama, passLama, function (err, isMatch) {
      if (err) {
        throw err;
      } else if (!isMatch) {
        Swal.fire({
          icon: "error",
          title: "Password Tidak sama dengan yang sebelumnya",
          showConfirmButton: false,
          timer: 1500,
        });
      } else {
        if (passBaru === conPassLama) {
          Swal.fire({
            icon: "error",
            title: "Password tidak boleh sama dengan sebelumnya",
            showConfirmButton: false,
            timer: 1500,
          });
        } else {
          if (passBaru === conPassBaru) {
            axios
              .put(
                "http://localhost:3089/uks/password/" +
                  localStorage.getItem("userId"),
                {
                  password: passBaru,
                }
              )
              .then(() => {
                Swal.fire({
                  icon: "success",
                  title: " Berhasil!!!",
                  showConfirmButton: false,
                  timer: 1500,
                });
                setTimeout(() => {
                  window.location.reload();
                }, 1500);
              })
              .catch((err) => {
                Swal.fire({
                  icon: "error",
                  title:
                    "Password minimal 8-20 karater, angka, huruf kecil & besar",
                  showConfirmButton: false,
                  timer: 1500,
                });
                console.log(err);
              });
          } else {
            Swal.fire({
              icon: "error",
              title: "Password Tidak sama",
              showConfirmButton: false,
              timer: 1500,
            });
          }
        }
      }
    });
  };

  const togglePassword = () => {
    console.log(passwordType);
    if (passwordType === "password") {
      setPasswordType("text");
      return;
    }
    setPasswordType("password");
  };

  const togglePassword1 = () => {
    if (passwordType1 === "password") {
      setPasswordType1("text");
      return;
    }
    setPasswordType1("password");
  };

  const togglePassword2 = () => {
    if (passwordType2 === "password") {
      setPasswordType2("text");
      return;
    }
    setPasswordType2("password");
  };
  return (
    <div>
      <Navbar />
      <div className="flex">
        <Sidebar />

        <form className="md:fixed md:ml-[15rem]">
          <div className="relative left-[7rem] md:left-[27%] top-[3%] md:top-[7rem] w-[40%] md:w-[25%]">
            <p className=" text-base md:text-3xl font-bold rounded-2xl bg-[#0c4a6e] md:p-2 p-1 text-white  text-center">
              Profile Saya
            </p>
          </div>
          <div className="ml-[10%] md:ml-[4%] -mt-2 md:mt-[6%] md:w-[91%] md:max-w-[91%] md:min-w-[91%] leading-10 items-center md:h-[20rem] h-[20rem] block md:flex font-bold w-[89%] ">
            <div className="text-center md:text-left md:w-[50rem] md:h-fit h-[30.5rem] border-black border p-5 rounded-2xl md:-mr-5 bg-gray-300/30 shadow-black shadow-2xl ">
              <p className="font-bold md:text-3xl text-lg h-12 md:mt-0 mt-[68%] "></p>
              <hr className="md:w-[80%] w-[48%]" />
              <p className=" md:text-base text-sm md:mt-3 mt-1">
                <i class="fa-solid fa-user text-[#0c4a6e]"></i>
                Username : 
                {profile.username}
              </p>
              <p className="md:mt-0 -mt-4 md:text-base text-sm">
                <i className="fas fa-envelope-open-text text-[#0c4a6e]"></i>{" "}
                Email : {profile.email}
              </p>
              <p className=" md:text-base text-sm md:mt-3 mt-1">
                <i className="fas fa-map-marked text-[#0c4a6e]"></i>
                Alamat : {profile.alamat ? (<div>{profile.alamat}</div>) : ("Default")}
              </p>
              <p className=" md:text-base text-sm md:mt-3 mt-1">
                <i className="fas fa-phone-alt text-[#0c4a6e]"></i> No Telephone
                : {profile.noTelp ? (<div>{profile.noTelp}</div>) : ("Default")}
              </p>
              {/* <p className="md:mt-0 mt-[10%] w-[140%] md:ml-0 -ml-[6%] sm:text-center md:text-left"> */}
              <p className="text-justify  md:text-base text-sm md:w-[100%] md:mt-7 mt-[7%]">
                Kelola informasi profil Anda untuk mengontrol, melindungi dan
                mengamankan akun
              </p>
              <hr />
            </div>
            <div className="md:min-h-[23rem] z-10 md:max-h-[23rem] md:h-[23rem] md:w-[27%] md:max-w-[27%] md:min-w-[27%]">
              {profile.foto ? (
                <img
                  className="md:shadow-black md:shadow-2xl w-[35%] border-[#0c4a6e] md:border-8 border-4 md:rounded-2xl md:static absolute md:bottom-0 bottom-[21.5rem] md:h-full md:w-full rounded-full md:left-0 left-[35%] cursor-pointer"
                  src={profile.foto}
                  alt=""
                  onClick={handleShow}
                />
              ) : (
                <img
                  className="md:shadow-black cursor-pointer md:shadow-2xl w-[35%] border-[#0c4a6e] md:border-8 border-4 md:rounded-2xl md:static absolute md:bottom-0 bottom-[21.5rem] md:h-full md:w-full rounded-full md:left-0 left-[30%]"
                  src="https://static.vecteezy.com/system/resources/previews/008/442/086/original/illustration-of-human-icon-user-symbol-icon-modern-design-on-blank-background-free-vector.jpg"
                  alt=""
                  onClick={handleShow}
                />
              )}
            </div>{" "}
          </div>
          <div className="flex md:w-[100%] gap-x-5 md:gap-x-2 relative md:-left-[35%]  -left-[10%] md:-mt-1 mt-[7.5rem]">
            <button
              className="ml-[30%] md:w-[16%] w-[20%] md:ml-[53%]  bottom-[8%] bg-[#0c4a6e] md:text-base text-sm   font-bold p-2 md:px-5 rounded-2xl"
              type="button"
            >
              <a href={"/editProfil/" + profile.id} className="text-white no-underline">
                <i className="fas fa-user-edit"></i> Edit
              </a>
            </button>
            <button
              className=" bg-[#0c4a6e] text-white font-bold px-3 rounded-xl"
              type="button"
              onClick={handleShow1}
            >
              <i className="fas fa-unlock-alt"></i> Edit Password
            </button>
            <button
              className="bg-[#0c4a6e] text-white font-bold px-3 rounded-xl"
              type="button"
            >
              <a href={"/register"} className="text-white no-underline">
              <i class="fa-sharp fa-solid fa-registered"></i> Buat Akun
              </a>
            </button>
          </div>
        </form>

        {/* Modal Profile */}
        <Modal
          show={show}
          onHide={handleClose}
          id="authentication-modal"
          tabIndex="-1"
          aria-hidden="true"
          className="md:ml-[30%] ml-0 fixed top-0 w-fit left-0 right-0 z-50 hidden p-4 overflow-x-hidden overflow-y-auto md:inset-0 focus:outline-none focus:ring-0 h-modal border-none md:h-full"
        >
          <div className="relative h-full md:h-auto focus:outline-none focus:ring-0 ">
            <div className="relative bg-white rounded-lg shadow">
              <button
                type="button"
                className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                data-modal-hide="authentication-modal"
                onClick={handleClose}
              >
                <svg
                  aria-hidden="true"
                  className="w-5 h-5"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  ></path>
                </svg>
                <span className="sr-only">Close modal</span>
              </button>
              <div className="px-6 py-6 lg:px-8">
                <h3 className="mb-4 text-xl font-medium text-gray-900">Edit Foto</h3>
                <form
                  onSubmit={put}
                  className="space-y-3"
                  // onSubmit={add}
                >
                  <div>
                    <label className="block mb-2 text-sm font-medium text-gray-900 ">
                      Foto Profile
                    </label>
                    <input
                      placeholder="Foto Profile"
                      className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1  dark:placeholder-gray-400 dark:text-black"
                      required
                      type="file"
                      onChange={(e) => setPhotoProfile(e.target.files[0])}
                    />
                  </div>
                  <button
                    type="submit"
                    className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    Edit
                  </button>
                </form>
              </div>
            </div>
          </div>
        </Modal>

        {/* Modal Password */}
        <Modal
          show={show1}
          onHide={handleClose1}
          id="authentication-modal"
          tabIndex="-1"
          aria-hidden="true"
          className="md:ml-[30%] ml-0 fixed top-0 left-0 right-0 z-50 hidden p-4 md:inset-0 h-modal md:h-full focus:outline-none focus:ring-0"
        >
          <div className="relative w-full h-full max-w-md md:h-auto focus:outline-none focus:ring-0">
            <div className="relative bg-white rounded-lg shadow">
              <button
                type="button"
                className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:text-white"
                data-modal-hide="authentication-modal"
                onClick={handleClose1}
              >
                <svg
                  aria-hidden="true"
                  className="w-5 h-5"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  ></path>
                </svg>
                <span className="sr-only">Close modal</span>
              </button>
              <div className="px-6 py-6 lg:px-8">
                <h3 className="mb-4 text-xl font-medium text-gray-900 dark:text-black">
                  Edit Password
                </h3>
                <form onSubmit={ubahPass} className="space-y-3">
                  <div>
                    <label htmlFor="password">Password Lama</label>
                    <div className="relative border-none focus:outline-none focus:ring-0">
                      <input
                        required
                        placeholder="Masukan password lama anda"
                        type={passwordType2}
                        className="md:h-5 w-full rounded-lg border p-4 pr-12 text-sm shadow-sm focus:outline-none focus:ring-0"
                        onChange={(e) => setConPassLama(e.target.value)}
                      />

                      <span
                        onClick={togglePassword2}
                        className="absolute inset-y-0 right-0 grid place-content-center px-4 focus:outline-none focus:ring-0"
                      >
                        {passwordType2 === "password" ? (
                          <>
                            <i className="fa-solid fa-eye-slash"></i>
                          </>
                        ) : (
                          <>
                            <i className="fa-solid fa-eye"></i>
                          </>
                        )}
                      </span>
                    </div>
                  </div>
                  <div>
                    <label htmlFor="password">Password Baru</label>
                    <div className="relative">
                      <input
                        required
                        placeholder="Masukan password baru"
                        type={passwordType}
                        className="md:h-5 w-full rounded-lg border p-4 pr-12 text-sm shadow-sm focus:outline-none focus:ring-0"
                        onChange={(e) => setPassBaru(e.target.value)}
                      />

                      <span
                        onClick={togglePassword}
                        className="absolute inset-y-0 right-0 grid place-content-center px-4  focus:outline-none focus:ring-0"
                      >
                        {passwordType === "password" ? (
                          <>
                            <i className="fa-solid fa-eye-slash"></i>
                          </>
                        ) : (
                          <>
                            <i className="fa-solid fa-eye"></i>
                          </>
                        )}
                      </span>
                    </div>
                  </div>
                  <div>
                    <label htmlFor="password">Konfirmasi Password</label>
                    <div className="relative">
                      <input
                        required
                        placeholder="Konfirmasi Password Baru"
                        type={passwordType1}
                        className="md:h-5 w-full rounded-lg border p-4 pr-12 text-sm shadow-sm  focus:outline-none focus:ring-0"
                        onChange={(e) => setConPassBaru(e.target.value)}
                      />

                      <span
                        onClick={togglePassword1}
                        className="absolute inset-y-0 right-0 grid place-content-center px-4  focus:outline-none focus:ring-0"
                      >
                        {passwordType1 === "password" ? (
                          <>
                            <i className="fa-solid fa-eye-slash"></i>
                          </>
                        ) : (
                          <>
                            <i className="fa-solid fa-eye"></i>
                          </>
                        )}
                      </span>
                    </div>
                  </div>
                  <button
                    onClick={handleClose}
                    type="submit"
                    className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    Edit
                  </button>
                </form>
              </div>
            </div>
          </div>
        </Modal>
      </div>
    </div>
  );
}
