import React, { useState } from "react";
import Navbar from "./Navbar";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

export default function () {
  let time = new Date().toLocaleTimeString();
  const [currentTime, setCurrentTime] = useState(time);

  const updateTime = () => {
    let time = new Date().toLocaleTimeString();
    setCurrentTime(time);
  };

  setInterval(updateTime, 1000);

  const navigate = useNavigate();

  const logout = () => {
    Swal.fire({
      title: "Anda Yakin Ingin Logout",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          icon: "success",
          title: "Berhasil Logout",
          showConfirmButton: false,
          timer: 1500,
        });
        navigate("/register");
        setTimeout(() => {
          window.location.reload();
        }, 1500);
        localStorage.clear();
      }
    });
  };
  return (
    <div className="">
      {/* <Navbar/> */}
      <button
        data-drawer-target="default-sidebar"
        data-drawer-toggle="default-sidebar"
        aria-controls="default-sidebar"
        type="button"
        class="inline-flex items-center p-2 mt-2 ml-3 text-sm text-white rounded-lg sm:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
      >
        <span class="sr-only">Open sidebar</span>
        <svg
          class="w-6 h-6"
          aria-hidden="true"
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            clip-rule="evenodd"
            fill-rule="evenodd"
            d="M2 4.75A.75.75 0 012.75 4h14.5a.75.75 0 010 1.5H2.75A.75.75 0 012 4.75zm0 10.5a.75.75 0 01.75-.75h7.5a.75.75 0 010 1.5h-7.5a.75.75 0 01-.75-.75zM2 10a.75.75 0 01.75-.75h14.5a.75.75 0 010 1.5H2.75A.75.75 0 012 10z"
          ></path>
        </svg>
      </button>

      <aside
        id="default-sidebar"
        class="fixed top-0 left-0 z-40 w-64 h-screen transition-transform -translate-x-full sm:translate-x-0 "
        aria-label="Sidebar"
      >
        <div class="h-full py-4 overflow-y-auto bg-black text-white">
          <div
            class="items-center  justify-between hidden w-full md:flex pl-4 md:order-1"
            id="mobile-menu-2"
          >
            <h1 className="text-xl font-semibold text-cyan-600">
              Sistem Aplikasi UKS
            </h1>
          </div>
          <hr className="mt-4" />
          <ul class="space-y-2 mr-3 font-medium">
            <li>
              <a
                href="/dashboard"
                class="flex items-center p-2 text-white rounded-lg dark:text-white hover:bg-cyan-600 no-underline dark:hover:bg-gray-700 mt-3"
              >
                <svg
                  aria-hidden="true"
                  class="w-6 h-6 text-white transition duration-75 dark:text-white group-hover:text-gray-900 dark:group-hover:text-white"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M2 10a8 8 0 018-8v8h8a8 8 0 11-16 0z"></path>
                  <path d="M12 2.252A8.014 8.014 0 0117.748 8H12V2.252z"></path>
                </svg>
                <span class="ml-3">Dashboard</span>
              </a>
            </li>
            <li>
              <a
                href="/periksa"
                class="flex items-center p-2 text-white rounded-lg dark:text-white hover:bg-cyan-600 no-underline dark:hover:bg-gray-700"
              >
                <i class="fa-solid fa-stethoscope"></i>
                <span class="flex-1 ml-3 whitespace-nowrap">
                  Periksa Pasien
                </span>
              </a>
            </li>
            {/* data */}
            <details class="group [&_summary::-webkit-details-marker]:hidden text-white">
              <summary class="overflow-hidden flex cursor-pointer items-center justify-between rounded-lg px-2 py-2 no-underline hover:bg-cyan-600 ">
                <div class="flex items-center gap-2">
                  <i className="fa-solid fa-book"></i>

                  <span class="text-sm font-medium"> Data </span>
                </div>

                <span class="shrink-0 transition duration-300 group-open:-rotate-180">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    class="h-5 w-5"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fill-rule="evenodd"
                      d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                      clip-rule="evenodd"
                    />
                  </svg>
                </span>
              </summary>

              <nav aria-label="Teams Nav" class="mt-2 flex flex-col px-3">
                <a
                  href="/guru"
                  class="text-white flex items-center gap-2 rounded-lg px-2 py-2 hover:bg-cyan-600 no-underline "
                >
                  <i class="fa-solid fa-chalkboard-user"></i>
                  <span class="text-sm font-medium">Daftar Guru </span>
                </a>

                <a
                  href="/siswa"
                  class="text-white flex items-center gap-2 rounded-lg px-2 py-2 hover:bg-cyan-600 no-underline"
                >
                  <i class="fa-solid fa-graduation-cap"></i>
                  <span class="text-sm font-medium">Daftar Siswa </span>
                </a>

                <a
                  href="/karyawan"
                  class="text-white flex items-center gap-2 rounded-lg px-2 py-2 hover:bg-cyan-600 no-underline"
                >
                  <i class="fa-solid fa-user-tie"></i>
                  <span class="text-sm font-medium">Daftar Karyawan </span>
                </a>
              </nav>
            </details>
            <li>
              <a
                href="/diagnosa"
                class="flex items-center p-2 text-white rounded-lg dark:text-white hover:bg-cyan-600 no-underline dark:hover:bg-gray-700"
              >
                <i class="fa-solid fa-person-dots-from-line"></i>
                <span class="flex-1 ml-3 whitespace-nowrap">Diagnosa</span>
              </a>
            </li>

            <li>
              <a
                href="/penanganan"
                class="flex items-center p-2 text-white rounded-lg dark:text-white hover:bg-cyan-600 no-underline dark:hover:bg-gray-700"
              >
                <i class="fa-solid fa-hand-holding-medical"></i>
                <span class="flex-1 ml-3 whitespace-nowrap">
                  Penangan Pertama
                </span>
              </a>
            </li>
            <li>
              <a
                href="/tindakan"
                class="flex items-center p-2 text-white rounded-lg dark:text-white hover:bg-cyan-600 no-underline dark:hover:bg-gray-700"
              >
                <i class="fa-solid fa-location-crosshairs"></i>{" "}
                <span class="flex-1 ml-3 whitespace-nowrap">Tindakan</span>
              </a>
            </li>
            <li>
              <a
                href="/obat"
                class="flex items-center p-2 text-white rounded-lg dark:text-white hover:bg-cyan-600 no-underline dark:hover:bg-gray-700"
              >
                <i class="fa-solid fa-suitcase-medical"></i>
                <span class="flex-1 ml-3 whitespace-nowrap">
                  Daftar Obat P3K
                </span>{" "}
              </a>
            </li>
          </ul>
          <div className="fixed bottom-2 pl-6 w-[90%]">
            <button onClick={logout}
              class="flex items-center p-2 text-white rounded-lg dark:text-white hover:bg-cyan-600 no-underline dark:hover:bg-gray-700"
            >
              <svg
                aria-hidden="true"
                class="flex-shrink-0 w-6 h-6 text-white transition duration-75 dark:text-white group-hover:text-gray-900 dark:group-hover:text-white"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill-rule="evenodd"
                  d="M3 3a1 1 0 00-1 1v12a1 1 0 102 0V4a1 1 0 00-1-1zm10.293 9.293a1 1 0 001.414 1.414l3-3a1 1 0 000-1.414l-3-3a1 1 0 10-1.414 1.414L14.586 9H7a1 1 0 100 2h7.586l-1.293 1.293z"
                  clip-rule="evenodd"
                ></path>
              </svg>
              <span class="flex-1 ml-3 whitespace-nowrap">Logout</span>
            </button>
          </div>
        </div>
      </aside>
    </div>
  );
}
