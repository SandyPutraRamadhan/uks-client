import React, { Component, useState } from "react";
import "../Css/Login.css";
import axios from "axios";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate();

  const login = async (e) => {
    e.preventDefault();

    try {
      const { data, status } = await axios.post(
        "http://localhost:3089/uks/login",
        {
          email: email,
          password: password,
        }
      );
      // Jika respon 200/ ok
      if (status === 200) {
        Swal.fire({
          icon: "success",
          title: "Login Berhasil!!!",
          showConfirmButton: false,
          timer: 1500,
        });
        localStorage.setItem("userId", data.data.user.id);
        navigate("/home");
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Username atau password tidak valid!",
        showConfirmButton: false,
        timer: 1500,
      });
      console.log(error);
    }
  };
  return (
    <div>
      <div class="wrapper">
        <div class="logo">
          <img
            src="http://103.133.27.106:3000/static/media/smpn1smg.174af4089a0f0045f9a4.png"
            alt=""
          />
        </div>
        <div class="text-center mt-4 name">Sistem Aplikasi UKS</div>
        <form class="p-3 mt-3" onSubmit={login}>
          <div class="form-field d-flex align-items-center">
            <input
              type="text"
              name="userName"
              id="userName"
              placeholder="Username"
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div class="form-field d-flex align-items-center">
            <input
              type="password"
              name="password"
              id="pwd"
              placeholder="Password"
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <button class="btn mt-3" type="submit">
            Login
          </button>
        </form>
        
      </div>
    </div>
  );
}
