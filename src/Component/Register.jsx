import React, {useState} from 'react'
import { useNavigate } from 'react-router-dom';
import "../Css/Login.css"
import Swal from 'sweetalert2';
import axios from 'axios';

export default function Register() {
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  
  const navigate = useNavigate();

  const register = async (e) => {
    e.preventDefault();
    try {
      await axios.post(
        "http://localhost:3089/uks/register", {
        email: email,
        username: username,
        password: password,
      }
      )
        .then(() => {
          Swal.fire({
            icon: 'success',
            title: 'Berhasil Registrasi',
            showConfirmButton: false,
            timer: 1500
          })
          setTimeout(() => {
            navigate('/')
            window.location.reload();
          }, 1500)
        })
    } catch (error) {
      alert("Terjadi Kesalahan " + error)
    }
  }
  return (
    <div>
    <div class="wrapper">
      <div class="logo">
        <img
          src="http://103.133.27.106:3000/static/media/smpn1smg.174af4089a0f0045f9a4.png"
          alt=""
        />
      </div>
      <div class="text-center mt-4 name">Sistem Aplikasi UKS</div>
      <form class="p-3 mt-3" onSubmit={register}>
      <div class="form-field d-flex align-items-center">
          <input
            type="text"
            name="userName"
            id="userName"
            placeholder="Username"
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        <div class="form-field d-flex align-items-center">
          <input
            type="text"
            name="userName"
            id="userName"
            placeholder="Email"
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div class="form-field d-flex align-items-center">
          <input
            type="password"
            name="password"
            id="pwd"
            placeholder="Password"
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <button class="btn mt-3" type="submit">
          Register
        </button>
      </form>
      <div class="text-center fs-6">
          or <a href="/">Sign In</a>
        </div>
    </div>
  </div>
  )
}
