import React, { useState, useEffect } from "react";
import Navbar from "./Navbar";
import Sidebar1 from "./Sidebar";
import axios from "axios";

export default function Dashboard() {
  const [guru, setGuru] = useState([]);
  const getAll = async () => {
    await axios
      .get(`http://localhost:3089/data/all?status=guru`)
      .then((res) => {
        setGuru(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const [siswa, setSiswa] = useState([]);

  const getAll1 = async () => {
    await axios
      .get(`http://localhost:3089/data/all?status=siswa`)
      .then((res) => {
        setSiswa(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const [karyawan, setKaryawan] = useState([]);

  const getAll2 = async () => {
    await axios
      .get(`http://localhost:3089/data/all?status=karyawan`)
      .then((res) => {
        setKaryawan(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const [pasien , setPasien] = useState([])

  const getAllPasien = async () => {
    await axios
      .get("http://localhost:3089/pasien/all-pasien")
      .then((res) => {
        setPasien(res.data.data);
      });
  };

  useEffect(() => {
    getAll(0);
    getAll1(0);
    getAll2(0);
    getAllPasien()
  }, []);
  return (
    <div>
      <Navbar />
      <div className="flex">
        <Sidebar1 />
        <div className="pl-64 w-full">
          <div className="p-3 md:overflow-y-hidden overflow-y-auto">
            <div className="md:flex md:flex-row gap-2">
              <div className="md:w-[25rem] mt-8 ml-[1px] md:ml-2 bg-cyan-600 text-white md:h-[9rem] h-[7.5rem]">
                <div className="w-auto">
                  <div className="flex gap-3 md:gap-1 justify-between items-center px-4">
                    <div>
                      <p className="text-sm md:text-lg pt-2">
                        Daftar Pasien Guru
                      </p>
                      <hr className="bg-green-600 h-1" />
                      <p className="text-lg font-bold md:text-2xl pt-2">
                        <span>
                          <i class="fa-solid fa-wheelchair"></i> {guru.length}{" "}
                          Guru
                        </span>
                      </p>
                    </div>
                    <i class="fa-solid fa-chalkboard-user text-6xl mt-5 text-cyan-700 hover:scale-110 duration-300"></i>
                  </div>
                </div>
              </div>

              <div className="md:w-[25rem] mt-8 ml-[1px] md:ml-2 bg-cyan-600 text-white md:h-[9rem] h-[7.5rem]">
                <div className="w-auto">
                  <div className="flex gap-3 md:gap-1 justify-between items-center px-4">
                    <div>
                      <p className="text-sm md:text-lg pt-2">
                        Daftar Pasien Siswa
                      </p>
                      <hr className="bg-green-600 h-1" />
                      <p className="text-lg font-bold md:text-2xl pt-2">
                        <i class="fa-solid fa-wheelchair"></i> {siswa.length}{" "}
                        Siswa
                      </p>
                    </div>
                    <i class="fa-solid fa-graduation-cap text-6xl mt-5 text-cyan-700 hover:scale-110 duration-300"></i>
                  </div>
                </div>
              </div>

              <div className="md:w-[25rem] mt-8 ml-[1px] md:ml-2 bg-cyan-600 text-white md:h-[9rem] h-[7.5rem]">
                <div className="w-auto">
                  <div className="flex gap-3 md:gap-1 justify-between items-center px-4">
                    <div>
                      <p className="text-sm md:text-lg pt-2">
                        Daftar Pasien Karyawan
                      </p>
                      <hr className="bg-green-600 h-1" />
                      <p className="text-lg font-bold md:text-2xl pt-2">
                        <i class="fa-solid fa-wheelchair"></i> {karyawan.length}{" "}
                        Karyawan
                      </p>
                    </div>
                    <i class="fa-solid fa-user-tie text-6xl mt-5 text-cyan-700 hover:scale-110 duration-300"></i>
                  </div>
                </div>
              </div>
            </div>
            <br />
            {/* table */}
            <h1 className="p-3">
              <div className="shadow-xl mt-3">
                <h1 className="">
                  <div className="text-center bg-cyan-600 p-3 rounded-t-md">
                    <h3 className="text-white text-2xl">Riwayat Pasien</h3>
                  </div>
                  <div class="flex flex-col px-3">
                    <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
                      <div class="inline-block min-w-full py-2 sm:px-6 lg:px-8">
                        <div class="overflow-hidden">
                          <table class="min-w-full text-center text-sm font-light">
                            <thead class="border-b bg-white font-medium dark:border-neutral-500 dark:bg-neutral-600">
                              <tr>
                                <th scope="col" class="px-6 py-3">
                                  No.
                                </th>
                                <th scope="col" class="px-6 py-3">
                                  Nama Pasien
                                </th>
                                <th scope="col" class="px-6 py-3">
                                  Status Pasien
                                </th>
                                <th scope="col" class="px-6 py-3">
                                  Tanggal/Jam Periksa
                                </th>
                              </tr>
                            </thead>
                            <tbody>
                              {pasien.map((pasiens, index) => {
                                return (
                              <tr key={index.id} class="border-b bg-neutral-100 dark:border-neutral-500 dark:bg-neutral-700">
                                <td class="whitespace-nowrap px-6 py-3 font-medium">
                                  {index + 1}
                                </td>
                                <td class="whitespace-nowrap px-6 py-3">
                                {pasiens.data.nama}
                                </td>
                                <td class="whitespace-nowrap px-6 py-3">
                                  {pasiens.data.status}
                                </td>
                                <td class="whitespace-nowrap px-6 py-3">
                                {pasiens.tglPeriksa}
                                </td>
                              </tr>
                                )
                              })}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </h1>
              </div>
            </h1>
          </div>
        </div>
      </div>
    </div>
  );
}
