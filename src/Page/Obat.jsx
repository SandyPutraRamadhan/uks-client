import React, { useState, useEffect } from "react";
import Sidebar from "../Component/Sidebar";
import axios from "axios";
import Navbar from "../Component/Navbar";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Swal from "sweetalert2";

export default function Obat() {
  const [obat, setObat] = useState([]);
  const [namaObat, setNamaObat] = useState("");
  const [stock, setStock] = useState(Number);
  const [tanggalExpired, setTanggalExpired] = useState("");
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const getAll = async () => {
    await axios
      .get(`http://localhost:3089/obat`)
      .then((res) => {
        setObat(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAll(0);
  }, []);

  const add = async (e) => {
    e.preventDefault();
    e.persist();

    try {
      await axios.post("http://localhost:3089/obat", {
        namaObat: namaObat,
        stock: stock,
        tanggalExpired: tanggalExpired,
      });
      Swal.fire({
        icon: "success",
        title: "Sukses Menambahkan",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const deleteObat = async (id) => {
    Swal.fire({
      title: "Apakah Anda Ingin Menghapus?",
      text: "Perubahan data tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Hapus",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`http://localhost:3089/obat/` + id);
        Swal.fire({
          icon: "success",
          title: "Dihapus!",
          showConfirmButton: false,
          timer: 1500,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    });
  };
  return (
    <div>
      <Navbar />
      <div className="flex p-10">
        <Sidebar />
        <div className="pl-64 w-full">
          <div className="shadow">
            <div className="flex gap-3 justify-between bg-cyan-600 py-3 px-3 rounded-t-md  text-white">
              <h3>Daftar Obat</h3>
              <div className="ml-auto">
                <button
                  className="w-48 bg-green-500 hover:bg-blue-700 text-white font-bold py-1 px-2 rounded-full"
                  onClick={handleShow}
                >
                  Tambah
                </button>
              </div>
            </div>
            <div class="flex flex-col px-3">
              <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="inline-block min-w-full py-2 sm:px-6 lg:px-8">
                  <div class="overflow-hidden">
                    <table class="min-w-full text-left text-sm font-light">
                      <thead class="border-b bg-white font-medium dark:border-neutral-500 dark:bg-neutral-600">
                        <tr>
                          <th scope="col" class="px-6 py-3">
                            No.
                          </th>
                          <th scope="col" class="px-6 py-3">
                            Nama Obat
                          </th>
                          <th scope="col" class="px-6 py-3">
                            Stock
                          </th>
                          <th scope="col" class="px-6 py-3">
                            Aksi
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        {obat.map((obats, index) => {
                          return (
                            <tr
                              class="border-b bg-neutral-100 dark:border-neutral-500 dark:bg-neutral-700"
                              key={index.id}
                            >
                              <td class="whitespace-nowrap px-6 py-3 font-medium">
                                {index + 1}
                              </td>
                              <td class="whitespace-nowrap px-6 py-3">
                                {obats.namaObat}
                              </td>
                              <td class="whitespace-nowrap px-6 py-3">
                                {obats.stock}
                              </td>
                              <td class="whitespace-nowrap px-6 py-3">
                                <button class="bg-green-600 hover:bg-green-700 font-bold py-1 px-2 rounded">
                                  <a href={"/editObat/" + obats.id}>
                                    <i class="fa-solid fa-pen-to-square text-white"></i>
                                  </a>
                                </button>
                                <button
                                  onClick={() => deleteObat(obats.id)}
                                  class="bg-red-600 ml-2 hover:bg-red-700 text-white font-bold py-1 px-2 rounded"
                                >
                                  <i class="fa-solid fa-trash-can"></i>
                                </button>
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* Modal Tambah */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Tambah Obat</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={add}>
            <div>
              <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-black dark:text-black">
                Nama Obat
              </label>
              <input
                placeholder="Nama Obat"
                value={namaObat}
                onChange={(e) => setNamaObat(e.target.value)}
                className="bg-white border mb-3 border-gray-300 text-black md:text-base text-sm  focus:ring-blue-500 focus:border-blue-500 block w-full rounded md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
              ></input>
            </div>
            <div>
              <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-black dark:text-black">
                Stock
              </label>
              <input
                placeholder="Stock"
                type="number"
                value={stock}
                onChange={(e) => setStock(e.target.value)}
                className="bg-white border mb-3 border-gray-300 text-black md:text-base text-sm  focus:ring-blue-500 focus:border-blue-500 block w-full rounded md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
              ></input>
            </div>
            <div>
              <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-black dark:text-black">
                Tanggal Expired
              </label>
              <input
                placeholder="Tanggal Expired"
                type="date"
                value={tanggalExpired}
                onChange={(e) => setTanggalExpired(e.target.value)}
                className="bg-white border mb-3 border-gray-300 text-black md:text-base text-sm  focus:ring-blue-500 focus:border-blue-500 block w-full rounded md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                required
              ></input>
            </div>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Batal
              </Button>
              <Button variant="primary" type="submit">
                Simpan
              </Button>
            </Modal.Footer>
          </form>
        </Modal.Body>
      </Modal>
    </div>
  );
}
