import React, { useState, useEffect } from "react";
import Navbar from "../Component/Navbar";
import Sidebar from "../Component/Sidebar";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Swal from "sweetalert2";
import axios from "axios";

export default function () {
  const [pasien, setPasien] = useState([]);
  const [dataPasien, setDataPasien] = useState([]);
  const [status, setStatus] = useState("");
  const [keluhan, setKeluhan] = useState("");
  const [data, setData] = useState(0);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const getAllPasien = async () => {
    await axios
      .get("http://localhost:3089/pasien/all-pasien", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        setPasien(res.data.data);
      });
  };

  const getAllGuru = async () => {
    const guru = await axios.get(
      `http://localhost:3089/data/all?status=guru`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    );
    setDataPasien(guru.data.data);
  };

  const getAllSiswa = async () => {
    const siswa = await axios.get(
      `http://localhost:3089/data/all?status=siswa`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    );
    setDataPasien(siswa.data.data);
  };

  // method get all karyawan
  const getAllKaryawan = async () => {
    const karyawan = await axios.get(
      `http://localhost:3089/data/all?status=karyawan`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    );
    setDataPasien(karyawan.data.data);
  };

  // mathod status for pasien
  const changeStatusPasien = (e) => {
    setStatus(e.target.value);

    if (e.target.value === "siswa") {
      getAllSiswa();
    } else if (e.target.value === "karyawan") {
      getAllKaryawan();
    } else {
      getAllGuru();
    }
  };

  const add = async (e) => {
    e.preventDefault();
    try {
      await axios.post(
        "http://localhost:3089/pasien",
        {
          data: data,
          keluhan: keluhan,
          status: status,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Sukses Menambahkan Pasien",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    } catch (error) {
      alert("Terjadi Kesalahan " + error);
    }
  };

  const download = async (id) => {
    await axios({
      url: `http://localhost:3089/data/api/excelPasien/download/${id}`,
      method: "GET",
      responseType: "blob",
    }).then((response) => {
      setTimeout(() => {
        var fileURL = window.URL.createObjectURL(new Blob([response.data]));
        var fileLink = document.createElement("a");
        fileLink.href = fileURL;
        fileLink.setAttribute("download", "periksa.xlsx");
        document.body.appendChild(fileLink);
        fileLink.click();
      }, 2000);
    });
  };



  // menampung data
  useEffect(() => {
    getAllPasien();
  }, []);

  return (
    <div>
      <Navbar />
      <div className="flex">
        <Sidebar />
        <div className="w-full pl-64">
          <div className="p-3">
            {/* rekap data */}
            <div className="shadow">
              <div className="flex gap-3 justify-between bg-cyan-600 p-3 rounded-t-md  text-white">
                <h3>Filter Tanggal</h3>
                <div className="ml-auto">
                  <button className="w-48 bg-green-500 hover:bg-blue-700 text-white font-bold py-1 px-2 rounded-full">
                    Tambah
                  </button>
                </div>
              </div>
              <div class="container flex flex-col items-center justify-center px-5 mx-auto my-8 space-y-8 text-center sm:max-w-md">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 512 512"
                  class="w-40 h-40 dark:text-gray-600"
                >
                  <path
                    fill="currentColor"
                    d="M256,16C123.452,16,16,123.452,16,256S123.452,496,256,496,496,388.548,496,256,388.548,16,256,16ZM403.078,403.078a207.253,207.253,0,1,1,44.589-66.125A207.332,207.332,0,0,1,403.078,403.078Z"
                  ></path>
                  <rect
                    width="176"
                    height="32"
                    x="168"
                    y="320"
                    fill="currentColor"
                  ></rect>
                  <polygon
                    fill="currentColor"
                    points="210.63 228.042 186.588 206.671 207.958 182.63 184.042 161.37 162.671 185.412 138.63 164.042 117.37 187.958 141.412 209.329 120.042 233.37 143.958 254.63 165.329 230.588 189.37 251.958 210.63 228.042"
                  ></polygon>
                  <polygon
                    fill="currentColor"
                    points="383.958 182.63 360.042 161.37 338.671 185.412 314.63 164.042 293.37 187.958 317.412 209.329 296.042 233.37 319.958 254.63 341.329 230.588 365.37 251.958 386.63 228.042 362.588 206.671 383.958 182.63"
                  ></polygon>
                </svg>
                <p class="text-3xl pb-5">
                  Filter terlebih dahulu sesuai tanggal yang diinginkan.
                </p>
              </div>

              {/* <h1 className="text-center p-4 rounded-b">
                <p className="text-4xl font-semibold">Rekap Data Pasien</p>
                <p className="mt-3 text-lg font-semibold">
                  (2023-05-05) - (2023-05-05)
                </p>
                <button
                  className="btn btn-primary bg-green-600 w-[30%] p-2 mt-2"
                  type="button"
                >
                  Download Rekap Data Pasien
                </button>
              </h1> */}
            </div>

            {/* table */}
            <div className="shadow mt-4">
              <div className="flex gap-3 justify-between bg-cyan-600 p-3 rounded-t-md  text-white">
                <h3>Daftar Pasien</h3>
                <div className="ml-auto">
                  <button onClick={handleShow} className="w-48 bg-green-500 hover:bg-blue-700 text-white font-bold py-1 px-2 rounded-full">
                    Tambah
                  </button>
                </div>
              </div>
              <h1 className="p-3 rounded-b">
                <div class="flex flex-col">
                  <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="inline-block min-w-full py-2 sm:px-6 lg:px-8">
                      <div class="overflow-hidden">
                        <table class="min-w-full text-center text-sm font-light">
                          <thead class="border-b bg-white font-medium dark:border-neutral-500 dark:bg-neutral-600">
                            <tr>
                              <th scope="col" class="px-6 py-3">
                                No.
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Nama Pasien
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Status Pasien
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Jabatan
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Tanggal/Jam
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Keterangan
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Status
                              </th>
                              <th scope="col" class="px-6 py-3">
                                Aksi
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            {pasien.map((pasiens, index) => {
                              return (
                                <tr key={index.id} class="border-b bg-neutral-100 dark:border-neutral-500 dark:bg-neutral-700">
                                  <td class="whitespace-nowrap px-6 py-3 font-medium">
                                    {index + 1}
                                  </td>
                                  <td class="whitespace-nowrap px-6 py-3">{pasiens.data.nama}</td>
                                  <td class="whitespace-nowrap px-6 py-3">
                                    {pasiens.data.status}
                                  </td>
                                  <td class="whitespace-nowrap px-6 py-3">
                                    {pasiens.data.jabatan}
                                  </td>
                                  <td class="whitespace-nowrap px-6 py-3">{pasiens.tglPeriksa}</td>
                                  <td class="whitespace-nowrap px-6 py-3">{pasiens.keluhan}</td>
                                  <td class="whitespace-nowrap px-6 py-3">{pasiens.penanganan === "Belum Ditangani" ? (
                                    <p className="text-red-500 mt-3">Belum Ditangani</p>
                                  ) : (
                                    <p className="text-green-500 mt-3">
                                      Sudah Ditangani
                                    </p>
                                  )}</td>
                                  <td class="whitespace-nowrap px-6 py-3">
                                  {pasiens.penanganan === "Belum Ditangani" ? (
                            <p></p>
                          ) : (
                            <button onClick={() => download(pasiens.id)}>
                            <i class="fa-solid fa-cloud-arrow-down text-[25px]"></i>
                            </button>
                          )}

                                  {pasiens.penanganan === "Belum Ditangani" ? (
                            <a
                              href={"/statusPeriksa/" + pasiens.id}
                              className="text-xs no-underline"
                            >
                              <p className="text-white mt-3 py-2 rounded-md px-2 bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300">
                                Tangani
                              </p>
                            </a>
                          ) : (
                            <p className="text-white mt-3 ml-2 py-2 rounded-md px-2 bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300">
                              Selesai
                            </p>
                          )}   </td>
                                </tr>
                              )
                            })}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </h1>
            </div>
          </div>
        </div>
      </div>
      {/* Modal Tambah */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Tambah Daftar Pasien</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={add}>
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-44">
                Status Pasien
              </label>
              <select
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-2 border-[#292929]"
                onChange={(e) => changeStatusPasien(e)}
              >
                <option selected>Pilih Status : </option>
                <option value="guru">Guru</option>
                <option value="siswa">Siswa</option>
                <option value="karyawan">Karyawan</option>
              </select>
            </div>
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-44">
                Nama Pasien
              </label>
              <select
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-2 border-[#292929]"
                onChange={(e) => setData(e.target.value)}
              >
                <option selected>Pilih Nama : </option>
                {dataPasien.map((dataPasiens) => (
                  <option value={dataPasiens.id}>{dataPasiens.nama}</option>
                ))}{" "}
              </select>
            </div>
            <div className="flex justify-center items-center mb-3">
              <label className="text-sm font-medium text-black w-44">
                Keluhan Pasien
              </label>
              <input
                placeholder="Keluhan Pasien"
                onChange={(e) => setKeluhan(e.target.value)}
                value={keluhan}
                className="w-[100%] text-sm rounded-lg text-[#1d2b3a] py-1.5 px-1 border-2 border-[#292929]"
                required
              />
            </div>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Batal
              </Button>
              <Button variant="primary" type="submit">
                Simpan
              </Button>
            </Modal.Footer>
          </form>
        </Modal.Body>
      </Modal>
    </div>
  );
}
