import Sidebar from "../Component/Sidebar";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import PeriksaPasien from "./PeriksaPasien";

export default function () {
  const [pasien, setPasien] = useState({
    data: {},
  });
  const [penyakit, setPenyakit] = useState([]);
  const [penanganan, setPenanganan] = useState([]);
  const [tindakan, setTindakan] = useState([]);
  const [obat, setObat] = useState([]);
  const [statusPeriksa, setStatusPeriksa] = useState([]);

  const [namaObat, setNamaObat] = useState(0);
  const [namaPenyakit, setnamaPenyakit] = useState(0);
  const [namaTindakan, setNamaTindakan] = useState(0);
  const [namaPenanganan, setNamaPenanganan] = useState(0);

  const param = useParams();
  const navigate = useNavigate();

  const add = async (e) => {
    e.preventDefault();
    try {
      await axios.post(`http://localhost:3089/status-periksa`, {
        diagnosaId: namaPenyakit,
        obatId: namaObat,
        penangananId: namaPenanganan,
        tindakanId: namaTindakan,
      });
      Swal.fire({
        icon: "success",
        title: "Sukses Menambahkan",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    } catch (err) {
      console.log(err);
    }
  };

  const put = async (e) => {
    e.preventDefault();
    try {
      await axios.put(`http://localhost:3089/pasien/periksa/` + param.id, {
        penanganan: "Sudah ditangani",
      });
      Swal.fire({
        icon: "success",
        title: "Berhasil Mengedit",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        navigate("/periksa");
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const deleteStatus = async (id) => {
    Swal.fire({
      title: "Apakah Anda Ingin Menghapus?",
      text: "Perubahan data tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Hapus",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`http://localhost:3089/status-periksa/` + id);
        Swal.fire({
          icon: "success",
          title: "Dihapus!",
          showConfirmButton: false,
          timer: 1500,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    });
  };

  const allStatusPeriksa = async () => {
    await axios
      .get(`http://localhost:3089/status-periksa`)
      .then((res) => {
        setStatusPeriksa(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };
  console.log(statusPeriksa);

  const allDiagnosa = async () => {
    const diagnosa = await axios.get(`http://localhost:3089/diagnosa`, {
      
    });
    setPenyakit(diagnosa.data.data);
  };

  const allPenanganan = async () => {
    const penanganan = await axios.get(`http://localhost:3089/penanganan`, {
     
    });
    setPenanganan(penanganan.data.data);
  };

  const allTindakan = async () => {
    const tindakan = await axios.get(`http://localhost:3089/tindakan`, {
      
    });
    setTindakan(tindakan.data.data);
  };

  const allObat = async () => {
    const obat = await axios.get(`http://localhost:3089/obat`, {
      
    });
    setObat(obat.data.data);
  };

  useEffect(() => {
    allObat();
    allDiagnosa();
    allPenanganan();
    allTindakan();
    allStatusPeriksa();
  }, []);

  // menampung data
  useEffect(() => {
    axios
      .get("http://localhost:3089/pasien/" + param.id)
      .then((response) => {
        setPasien(response.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, [param.id]);
  return (
    <div>
      <Sidebar />
      <div className="pl-64 w-full">
        <div className="p-5">
          <h1 className="shadow">
            <h2 className="text-center bg-cyan-600 p-2 rounded-t-md text-white text-lg">
              Periksa Pasien : {pasien.data.nama}
            </h2>
            <h2>
              <h1 className="flex gap-2">
                <div>
                  <div>
                    <h1 className="text-sm font-semibold">Nama Pasien</h1>
                    <p className="bg-gray-100 rounded-md border border-gray-300 py-2 px-3 text-sm w-[28rem]">
                      {pasien.data.nama}
                    </p>
                  </div>
                </div>
                <div>
                  <h1 className="text-sm font-semibold">Status Pasien</h1>
                  <p className="bg-gray-100 rounded-md border border-gray-300 py-2 px-3 text-sm w-[28rem]">
                    {pasien.data.status}
                  </p>
                </div>
              </h1>
              <h1>
                <div>
                  <h1 className="text-sm font-semibold">Keluahan</h1>
                  <textarea>
                    <p className="bg-gray-100 rounded-md border border-gray-300 py-2 px-3 text-sm w-[28rem]">
                      {pasien.keluhan}
                    </p>
                  </textarea>
                </div>
              </h1>
              <form action="" className="p-3" onClick={add}>
                <h1 className="flex gap-3">
                  <div>
                    <label className="block md:mb-3 mb-2 text-base font-medium text-gray-900 dark:text-black">
                      Penyakit Pasien
                    </label>
                    <select
                      className="bg-gray-100 rounded-md border border-gray-300 py-2 px-2 text-sm w-[13.2rem]"
                      value={namaPenyakit}
                      onChange={(e) => setnamaPenyakit(e.target.value)}
                    >
                      <option selected>Pilih Penyakit : </option>
                      {penyakit.map((diagnosas) => (
                        <option value={diagnosas.id}>
                          {diagnosas.namaPenyakit}
                        </option>
                      ))}
                    </select>
                  </div>
                  <div>
                    <label className="block md:mb-3 mb-2 text-base font-medium text-gray-900 dark:text-black">
                      Penanganan Pertama
                    </label>
                    <select
                      className="bg-gray-100 rounded-md border border-gray-300 py-2 px-2 text-sm w-[13.2rem]"
                      value={namaPenanganan}
                      onChange={(e) => setNamaPenanganan(e.target.value)}
                    >
                      <option selected>Pilih Penanganan : </option>
                      {penanganan.map((penanganans) => (
                        <option value={penanganans.id}>
                          {penanganans.namaPenanganan}
                        </option>
                      ))}
                    </select>
                  </div>
                  <div>
                    <label className="block md:mb-3 mb-2 text-base font-medium text-gray-900 dark:text-black">
                      Tindakan
                    </label>
                    <select
                      className="bg-gray-100 rounded-md border border-gray-300 py-2 px-2 text-sm w-[13.2rem]"
                      value={namaTindakan}
                      onChange={(e) => setNamaTindakan(e.target.value)}
                    >
                      <option selected>Pilih Tindakan : </option>
                      {tindakan.map((tindakans) => (
                        <option value={tindakans.id}>
                          {tindakans.namaTindakan}
                        </option>
                      ))}
                    </select>
                  </div>
                  <div>
                    <div>
                      <label className="block md:mb-3 mb-2 text-base font-medium text-gray-900 dark:text-black">
                        Obat
                      </label>
                      <select
                        className="bg-gray-100 rounded-md border border-gray-300 py-2 px-2 text-sm w-[13.2rem]"
                        value={namaObat}
                        onChange={(e) => setNamaObat(e.target.value)}
                      >
                        <option selected>Pilih Obat : </option>
                        {obat.map((obats) => (
                          <option value={obats.id}>{obats.namaObat}</option>
                        ))}
                      </select>
                    </div>
                    <div>
                      <button
                        type="submit"
                        className="btn btn-primary w-[8rem] bg-cyan-600"
                      >
                        Simpan
                      </button>
                    </div>
                  </div>
                </h1>
              </form>
              {/* table */}
              <div className="mt-3">
                <div className="">
                  <div class="flex flex-col px-3 rounded-b-md">
                    <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
                      <div class="inline-block min-w-full py-2 sm:px-6 lg:px-8">
                        <div class="overflow-hidden">
                          <table class="min-w-full text-center text-sm font-light">
                            <thead class="border-b bg-white font-medium dark:border-neutral-500 dark:bg-neutral-600">
                              <tr>
                                <th scope="col" class="px-6 py-3">
                                  No.
                                </th>
                                <th scope="col" class="px-6 py-3">
                                  Nama Penyakit
                                </th>
                                <th scope="col" class="px-6 py-3">
                                  Tindakan
                                </th>
                                <th scope="col" class="px-6 py-3">
                                  Penanganan
                                </th>
                                <th scope="col" class="px-6 py-3">
                                  Obat
                                </th>
                                <th scope="col" class="px-6 py-3">
                                  Aksi
                                </th>
                              </tr>
                            </thead>
                            <tbody>
                              {statusPeriksa.map((PeriksaPasiens, index) => {
                                return (
                                  <tr
                                    key={index.id}
                                    class="border-b bg-neutral-100 dark:border-neutral-500 dark:bg-neutral-700"
                                  >
                                    <td class="whitespace-nowrap px-6 py-3 font-medium">
                                      {index + 1}
                                    </td>
                                    <td class="whitespace-nowrap px-6 py-3">
                                      {PeriksaPasiens.diagnosaId.namaPenyakit}
                                    </td>
                                    <td class="whitespace-nowrap px-6 py-3">
                                      {PeriksaPasiens.tindakanId.namaTindakan}
                                    </td>
                                    <td class="whitespace-nowrap px-6 py-3">
                                      {
                                        PeriksaPasiens.penangananId
                                          .namaPenanganan
                                      }
                                    </td>
                                    <td class="whitespace-nowrap px-6 py-3">
                                      {PeriksaPasiens.obatId.namaObat}
                                    </td>
                                    <td class="whitespace-nowrap px-6 py-3">
                                      <button
                                        onClick={() =>
                                          deleteStatus(PeriksaPasiens.id)
                                        }
                                        className="btn btn-danger"
                                      >
                                        Hapus
                                      </button>
                                    </td>
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </h2>
          </h1>
          <h1 className="shadow flex mt-3 p-3 rounded border">
            <p className="">
              <a href="/periksa">
                <button className="btn btn-danger w-[8rem] bg-red-600">
                  Kembali
                </button>
              </a>
            </p>
            <p className="ml-[44rem]">
              <button
                onClick={put}
                className="btn btn-primary w-[8rem] bg-cyan-600"
              >
                Selesai
              </button>
            </p>
          </h1>
        </div>
      </div>
    </div>
  );
}
