import {  Route, Routes   } from "react-router-dom";
import Login from './Component/Login';
import Home from "./Component/Home";
import Dashboard from "./Component/Dashboard";
import Guru from "./Data/Guru";
import Siswa from "./Data/Siswa";
import Karyawan  from "./Data/Karyawan";
import EditGuru from "./Data/EditData/EditGuru.jsx"
import EditSiswa from "./Data/EditData/EditSiswa.jsx"
import EditKaryawan from "./Data/EditData/EditKaryawan";
import EditDiagnosa from "./EditPage/EditDiagnosa";
import Diagnosa from "./Page/Diagnosa";
import PenangananPertama from "./Page/PenangananPertama";
import Tindakan from "./Page/Tindakan";
import EditPenangananPertama from "./EditPage/EditPenangananPertama";
import EditTindakan from "./EditPage/EditTindakan";
import Obat from "./Page/Obat";
import PeriksaPasien from "./Page/PeriksaPasien";
import Profile from "./Component/Profile";
import StatusPriksa from "./Page/StatusPriksa";
import Register from "./Component/Register";
import EditProfil from "./Data/EditData/EditProfil";
import EditObat from "./EditPage/EditObat";

function App() {
  return (
    <div>
      <Routes>
      <Route path='/' element={<Login/>}/>
      <Route path='/home' element={<Home/>}/>
      <Route path='/dashboard' element={<Dashboard/>}/>
      <Route path='/guru' element={<Guru/>}/>
      <Route path='/siswa' element={<Siswa/>}/>
      <Route path='/karyawan' element={<Karyawan/>}/>
      <Route path='/diagnosa' element={<Diagnosa/>}/>
      <Route path='/tindakan' element={<Tindakan/>}/>
      <Route path='/penanganan' element={<PenangananPertama/>}/>
      <Route path='/obat' element={<Obat/>}/>
      <Route path='/periksa' element={<PeriksaPasien/>}/>
      <Route path='/statusPeriksa/:id' element={<StatusPriksa/>}/>
      <Route path='/register' element={<Register/>}/>
      <Route path='/prof' element={<Profile/>}/>
      <Route path='/editGuru/:id' element={<EditGuru/>}/>
      <Route path='/editSiswa/:id' element={<EditSiswa/>}/>
      <Route path='/editKaryawan/:id' element={<EditKaryawan/>}/>
      <Route path='/editDiagnosa/:id' element={<EditDiagnosa/>}/>
      <Route path='/editPenanganan/:id' element={<EditPenangananPertama/>}/>
      <Route path='/editTindakan/:id' element={<EditTindakan/>}/>
      <Route path='/editProfil/:id' element={<EditProfil/>}/>
      <Route path='/editObat/:id' element={<EditObat/>}/>
      </Routes>
  </div>
  );
}

export default App;
