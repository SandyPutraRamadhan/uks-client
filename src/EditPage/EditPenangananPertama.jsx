import React, { useState, useEffect } from "react";
import Swal from "sweetalert2";
import { Params, useNavigate, useParams } from "react-router-dom";
import axios from "axios";

export default function EditPenangananPertama() {
  const [namaPenanganan, setNamaPenanganan] = useState("");

  const param = new useParams();

  const navigate = useNavigate();

  const Put = async (e) => {
    e.preventDefault();

    try {
      await axios.put(`http://localhost:3089/penanganan/` + param.id, {
        namaPenanganan: namaPenanganan,
      });
      Swal.fire({
        icon: "success",
        title: "Berhasil Mengedit",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        navigate("/penanganan");
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    axios
      .get("http://localhost:3089/penanganan/" + param.id)
      .then((response) => {
        const penanganan = response.data.data;
        setNamaPenanganan(penanganan.namaPenanganan);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, [param.id]);
  return (
    <div>
      <div className="">
        <div className="px-[25rem] py-[10rem]">
          <form className="space-y-3 shadow p-3 w-full" onSubmit={Put}>
            <h3 className="md:py-4 py-3 md:text-2xl text-xl font-medium text-black dark:text-black">
              Edit Penanganan Pertama
              <hr />
            </h3>
            <div>
              <div>
                <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-gray-900 dark:text-black">
                  Nama Penanganan
                </label>
                <input
                  placeholder="Nama Penanganan"
                  value={namaPenanganan}
                  onChange={(e) => setNamaPenanganan(e.target.value)}
                  className="bg-gray-50 mb-4 border w-full rounded border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-3 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                />
              </div>
              <div className="md:flex gap-[3rem]">
                <a
                  href="/penanganan"
                  className="w-[7rem] md:mr-10 mr-0 rounded float-right text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium text-sm ml-auto md:py-2.5 py-1.5 text-center no-underline dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
                >
                  Batal
                </a>
                <button
                  type="submit"
                  className="w-[7rem] md:float-right rounded text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium text-sm px-4 md:py-2.5 py-1.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Simpan
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
